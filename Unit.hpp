#pragma once
#include <SFML\Graphics.hpp>
#include "Vector2.hpp"
#include "Polygon.hpp"
#include <vector>

class Polygon;

class Unit
{
public:
	Unit(int x, int y, int size);
	void setMaxSpeed(float speed)
	{
		this->maxSpeed = speed;
	}

	float getCurrentSpeed()
	{
		return velocity.getLength();
	}

	Vector2 getLocation()
	{
		return location;
	}

	Vector2 getCenter()
	{
		float x = this->location.getX() + this->size / 2.0;
		float y = this->location.getY() + this->size / 2.0;
		return Vector2( x, y);
	}

	void input(sf::Event &event);

	void update(const std::vector<Unit*>& units, std::vector<Polygon*> &poly);

	Vector2  seek(const Vector2 target);

	Vector2 separate(const std::vector<Unit*>& units);
	Vector2 wallSeparate(std::vector<Polygon*> &poly);
	Vector2 wallSeek(Polygon* poly);

	void draw(sf::RenderWindow *window);


	void resetGoal()
	{
		this->hasGoal = false;
	}

	void setGoal(float x, float y)
	{
		hasGoal = true;
		goal = Vector2(x,y);
	}
	
	~Unit(void)
	{
	}

	void applyForce(Vector2 &force)
	{
		acceleration.plus(force);
	}

	void rotateByWall(Polygon* poly);



private:
	Vector2 location;
	Vector2 center;
	Vector2 velocity;
	Vector2 oldVelocity;
	Vector2 acceleration;
	int size;
	sf::IntRect area;
	bool hasGoal;
	Vector2 goal;
	float maxSpeed;
	float maxForce;
};


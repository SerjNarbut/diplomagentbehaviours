#pragma once
#include <cmath>
#include <cstring>
#include "Point.hpp"

class Vector2
{
public:

	Vector2()
	{
		this->x = 0;
		this->y = 0;
	}

	Vector2(float x, float y)
	{
		this->x = x;
		this->y = y;
	}

	Vector2(Point p){
		this->x = p.x;
		this->y = p.y;
	}

	Vector2(int x, int y)
	{
		this->x = static_cast<float>(x);
		this->y = static_cast<float>(y);
	}

	Vector2(const Vector2 &other)
	{
		this->x = other.x;
		this->y = other.y;
	}

	float getX() const
	{
		return this->x;
	}

	float getY() const
	{
		return this->y;
	}

	float getLength() const
	{
		return sqrtf(x*x + y*y);
	}

	float getSquaredLength() const
	{
		return x*x + y*y;
	}

	void normalize()
	{
		float len = getLength();
		if(len != 0)
		{
			this->x /= len;
			this->y /= len;
		}
	}

	void limit(float max)
	{
		if(getLength() > max)
		{
			normalize();
			this->x *= max;
			this->y *= max;
		}
	}

	void setLength(float value)
	{
		normalize();
		mult(value);
	}

	void plus(const Vector2 &other)
	{
		this->x += other.x;
		this->y += other.y;
	}

	void minus(const Vector2 &other)
	{
		this->x -= other.x;
		this->y -= other.y;
	}

	void mult(float val)
	{
		this->x *= val;
		this->y *= val;
	}

	void div(float val)
	{
		if(val != 0)
		{
			this->x /= val;
			this->y /= val;
		}
		else
			throw "div by zero";
	}

	void rotate(float angle){
		float sinAngle = sin(angle);
		float cosAngle = cos(angle);
		float x = this->x * cosAngle - this->y * sinAngle;
		float y = this->x *sinAngle + this->y*cosAngle;
		this->x = x;
		this->y = y;
	}

	float dotProduct(const Vector2 &other) const
	{
		return x*other.x + y*other.y;
	}

	float theta(const Vector2 &other) const
	{
		float dot = dotProduct(other);
		return acosf( dot / (getLength() * other.getLength()));
	}

	bool equals(const Vector2& other) const
	{
		return this->x == other.x && this->y == other.y;
	}

	Vector2& operator =(const Vector2 &other)
	{
		this->x = other.x;
		this->y = other.y;
		return *this;
	}

	std::string to_str()
	{
		std::string str1 = std::to_string(x);
		std::string str2 = std::to_string(y);
		return " " + str1 + " " + str2;

	}

	static Vector2 plus(const Vector2 &left, const Vector2 &right)
	{
		Vector2 vec(left);
		vec.plus(right);
		return vec;
	}

	static Vector2 minus(const Vector2 &left, const Vector2 &right)
	{
		Vector2 vec(left);
		vec.minus(right);
		return vec;
	}

	static float angleBetween(const Vector2 &left, const Vector2 &right)
	{
		float dot = left.dotProduct(right);
		return acosf(dot / (left.getLength() * right.getLength()));
	}

	static float distanceBetweenSquared(const Vector2 &vec1, const Vector2 &vec2)
	{
		return (vec2.x - vec1.x)*(vec2.x - vec1.x)  + (vec2.y - vec1.y)*(vec2.y - vec1.y);
	}

	static float distanceBetween(const Vector2 &vec1, const Vector2 &vec2)
	{
		return sqrtf( Vector2::distanceBetweenSquared(vec1,vec2));
	}

	static Vector2 zero()
	{
		return Vector2(0.0f,0.0f);
	}
	
	static Vector2 undef()
	{
		return Vector2(-1.0f,-1.0f);
	}


private:
	float x;
	float y;
};


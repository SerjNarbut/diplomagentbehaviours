#include "Unit.hpp"
#include <iostream>


Unit::Unit(int x, int y, int size)
{
	this->location = Vector2(x,y);
	this->center = Vector2(x + size / 2, y + size / 2);
	this->velocity = Vector2::zero();
	this->acceleration = Vector2::zero();

	this->size = size;
	this->hasGoal = false;
	this->goal = Vector2::undef();
	this->maxSpeed = 2;// rand() % 2 + 2;
	this->maxForce = 0.1f;
}

void Unit::update(const std::vector<Unit*>& units, std::vector<Polygon*> &poly)
{
	Vector2 separateForce = separate(units);
	Vector2 sepWall = wallSeparate(poly);
	if(separateForce.equals(Vector2::zero()) && !hasGoal && sepWall.equals(Vector2::zero()))
	{
		velocity.mult(0);
	}

	sepWall.mult(31.5f);
	separateForce.mult(5.5f);
	applyForce(sepWall);
	applyForce(separateForce);
	for (int i = 0; i < poly.size(); i++)
	{
		Vector2 acc = wallSeek(poly[i]);
		acc.mult(0.1);
		applyForce(acc);
	}
	

	if(hasGoal)
	{
		Vector2 seekForce = seek(goal);
		seekForce.mult(0.5f);
		applyForce(seekForce);

		if(Vector2::distanceBetween(this->getCenter(), goal) <= 3)
		{
			resetGoal();
			velocity.mult(0.f);
		}
	}

	

	velocity.plus(acceleration);
	velocity.limit(maxSpeed);
	for (int i = 0; i < poly.size(); i++)
	{
		rotateByWall(poly[i]);
	}
	location.plus(velocity);
	acceleration.mult(0.f);
}

Vector2 Unit::seek(const Vector2 target)
{
	Vector2 desired = Vector2::minus(target,this->getCenter());
	desired.normalize();
	desired.mult(maxSpeed);
	Vector2 steer = Vector2::minus(desired,velocity);
	steer.limit(maxForce);
	return steer;
}

Vector2 Unit::separate(const std::vector<Unit*>& units)
{
	float separateRadius = size *2.f;
	Vector2 sum = Vector2::zero();
	int count = 0;
	for(int i =0; i< units.size(); i++)
	{
		Vector2 newPos = Vector2::plus(getCenter(), velocity);
		float d = Vector2::distanceBetween(newPos, units[i]->getCenter());
		if( d >= 0 && d < separateRadius && units[i] != this)
		{
			units[i]->applyForce(this->acceleration);
			Vector2 difference = Vector2::minus(this->getCenter(), units[i]->getCenter());
			if(difference.equals(Vector2::zero()))
			{
				difference = Vector2(rand() % (int)this->getCenter().getX(), rand() % (int)this->getCenter().getY());
			}
			difference.normalize();
			if(d > 0)
				difference.div(d);
			sum.plus(difference);
			count++;
		}
	}
	if(count > 0)
	{
		sum.div(count);
		sum.normalize();
		sum.mult(maxSpeed);
		Vector2 steer = Vector2::minus(sum, velocity);
		steer.limit(maxForce);
		return steer;
	}
	return sum;
}

Vector2 Unit::wallSeparate(std::vector<Polygon*> &poly)
{
	float separateRadius = size *1.f;
	Vector2 sum = Vector2::zero();
	int count = 0;
	for(int i =0;  i< poly.size(); i++)
	{
		int index;
		Vector2 out;
		Vector2 newPos = Vector2::plus(getCenter(), velocity);
		float d = poly[i]->getMinDist(newPos, index, out);
		if( index == -1)
		{
			continue;
		}
		if( d > 0 && d < separateRadius)
		{
			count++;
			Vector2 difference = Vector2::minus(this->getCenter(), out);
			difference.div(d);
			sum.plus(difference);
		}
	}
	if(count > 0)
	{
		sum.normalize();
		sum.mult(maxSpeed);
		Vector2 steer = Vector2::minus(sum, velocity);
		steer.limit(maxForce);
		return steer;
	}
	return sum;
}

void Unit::draw(sf::RenderWindow *window)
{
	sf::VertexArray arr(sf::PrimitiveType::Quads,4);
	arr[0].position = sf::Vector2f(location.getX(),location.getY());
	arr[1].position = sf::Vector2f(location.getX() + size,location.getY());
	arr[2].position = sf::Vector2f(location.getX() + size,location.getY() + size);
	arr[3].position = sf::Vector2f(location.getX(),location.getY() + size);
	for(int i =0; i <4; i++)
	{
		arr[i].color = sf::Color(210,105,30);
	}
	sf::Vertex line[] =
	{
		sf::Vertex(sf::Vector2f(getCenter().getX(), getCenter().getY())),
		sf::Vertex(sf::Vector2f(getCenter().getX() + velocity.getX() * 30, getCenter().getY() + velocity.getY() * 30))
	};

	window->draw(line, 2, sf::Lines);
	window->draw(arr);
}

Vector2 Unit::wallSeek(Polygon* poly){
	Point start(getCenter().getX(), getCenter().getY());
	Vector2 moveTmp = this->velocity;
	Vector2 acc = Vector2::zero();
	Line direction = Line(start, Point(start.x + moveTmp.getX() * 10, start.y + moveTmp.getY() * 10));
	for (int i = 0; i < poly->getVertexCount(); i++){
		if (Line::intersect(direction, poly->getLine(i))){
			float d1 = Vector2::distanceBetweenSquared(Vector2(poly->getLine(i).p1), goal);
			float d2 = Vector2::distanceBetweenSquared(Vector2(poly->getLine(i).p2), goal);
			Vector2 start(poly->getLine(i).p1);
			Vector2 end(poly->getLine(i).p2);
			if (d2 < d1){
				start = Vector2(poly->getLine(i).p2);
				end = Vector2(poly->getLine(i).p1);
			}
			acc = Vector2::minus(start, end);
			break;
		}
	}
	return acc;
}

void Unit::rotateByWall(Polygon* poly){
	Point start(getCenter().getX(), getCenter().getY());
	Point end(goal.getX(), goal.getY());
	Line movement(start, end);
	bool ok = true;
	Vector2 moveTmp = this->velocity;
	Line direction = Line(start, Point(start.x + moveTmp.getX() * 40, start.y  + moveTmp.getY() * 40));
	for (int i = 0; i < poly->getVertexCount(); i++){
		while (Line::intersect(direction, poly->getLine(i))){
			direction = Line(start, Point(start.x + moveTmp.getX() * 40, start.y  + moveTmp.getY() * 40));
			float d1 = Vector2::distanceBetweenSquared(Vector2(poly->getLine(i).p1), goal);
			float d2 = Vector2::distanceBetweenSquared(Vector2(poly->getLine(i).p2), goal);
			float ang = 0.06;
			if (d2 < d1){
				ang = -0.06;
			}
			moveTmp.rotate(ang);
			ok = false;
		}
		if (!ok){
			this->velocity = moveTmp;
			break;
		}
	}
	if (hasGoal && ok)
	{
		Vector2 seekForce = seek(goal);
		seekForce.mult(0.5f);
		applyForce(seekForce);

		if (Vector2::distanceBetween(this->getCenter(), goal) <= 3)
		{
			resetGoal();
			velocity.mult(0.f);
		}
	}
}


void Unit::input(sf::Event &event)
{
}
#pragma once
#include <SFML\Graphics.hpp>
#include <vector>
#include <algorithm>
#include "Point.hpp"
#include "Line.hpp"
#include "Vector2.hpp"
#include "Unit.hpp"
class Unit;

class Polygon
{
protected:
	sf::ConvexShape *shape;
	sf::Color color;
	int vertexCount;
	std::vector<Point> vertexes;
	std::vector<Line> lines;
	Vector2 location;

public:
	Polygon(int vertCount, sf::Color color, Vector2 loc);
	explicit Polygon(std::vector<Point> vert, int count, sf::Color color, Vector2 loc);
	explicit Polygon(int *vert, int count, sf::Color, Vector2 loc);
	~Polygon(void);
	void draw(sf::RenderWindow *window);
	Vector2 getCenter();
	Vector2 getLocation();
	void setLocation(int x, int y);
	void setLocation(Vector2 loc);
	void setLocation(sf::Vector2f loc);
	sf::FloatRect getLocalBounds();
	void addPoint(int index, Point point);
	int getVertexCount() { return this->vertexCount;}
	Point getPoint(int index) {return vertexes[index];}
	Line getLine(int index) {return lines[index];}
	void buildLines();
	float getMinDist(Vector2 loc, int& index, Vector2 &out);
};


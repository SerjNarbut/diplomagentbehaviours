#pragma once
#include "Point.hpp"
#include "Vector2.hpp"
#include <cmath>
#include <cstring>

struct Line
{
	Point p1;
	Point p2;

	Line()
	{
		this->p1 = Point();
		this->p2= Point();
	}

	Line(Point p1, Point p2)
	{
		this->p1 = p1;
		this->p2 = p2;
	}

	Line(const Line& other)
	{
		this->p1 = other.p1;
		this->p2 = other.p2;
	}

	Line& operator=(const Line &other)
	{
		this->p1 = other.p1;
		this->p2 = other.p2;
		return *this;
	}

	bool normalOnLine(Vector2 point)
	{
		Vector2 self = Vector2(p2.x - p1.x, p2.y - p1.y);
		Vector2 pointVect = Vector2(point.getX() - p1.x, point.getY() - p1.y);
		float t = self.dotProduct(pointVect) / self.getSquaredLength();
		return (t >= 0 ) && (t <= 1);
	}

	float lenNormal(Vector2 point, Vector2 &out)
	{
		Vector2 self = Vector2(p2.x - p1.x, p2.y - p1.y);
		Vector2 pointVect = Vector2(point.getX() - p1.x, point.getY() - p1.y);
		float t = self.dotProduct(pointVect) / self.getSquaredLength();
		self.mult(t);
		out = Vector2(p1.x+ self.getX(), p1.y + self.getY());
		Vector2 normal = Vector2::minus(point, out);
		return normal.getLength();
	}

	static bool intersect(const Line lineOne, const Line lineTwo){
		Vector2 dir1 = Vector2::minus(Vector2(lineOne.p2), Vector2(lineOne.p1));
		Vector2 dir2 = Vector2::minus(Vector2(lineTwo.p2), Vector2(lineTwo.p1));

		float a1 = -dir1.getY();
		float b1 = +dir1.getX();
		float d1 = -(a1*lineOne.p1.x + b1*lineOne.p1.y);

		float a2 = -dir2.getY();
		float b2 = +dir2.getX();
		float d2 = -(a2*lineTwo.p1.x + b2*lineTwo.p1.y);

		float seg1_line2_start = a2*a1*lineOne.p1.x + b2*lineOne.p1.y + d2;
		float seg1_line2_end = a2*lineOne.p2.x + b2*lineOne.p2.y + d2;

		float seg2_line1_start = a1*lineTwo.p1.x + b1*lineTwo.p1.y + d1;
		float seg2_line1_end = a1*lineTwo.p2.x + b1*lineTwo.p2.y + d1;

		if (seg1_line2_start * seg1_line2_end >= 0 || seg2_line1_start * seg2_line1_end >= 0)
			return false;

		return true;
	}

	float getLenght()
	{
		return sqrtf((p1.x - p1.y) *(p1.x - p1.y) + (p2.x - p2.y) *(p2.x - p2.y));
	}

	float getLenghtNonSq()
	{
		return (p1.x - p1.y) *(p1.x - p1.y) + (p2.x - p2.y) *(p2.x - p2.y);
	}

	std::string to_str()
	{
		return "Point1={" + p1.to_str() + "}; Point2={"+p2.to_str() + "}";
	}
};
#include "Polygon.hpp"


Polygon::Polygon(int vertCount, sf::Color color, Vector2 loc)
{
	this->color = color;
	this->vertexCount = vertCount;
	this->vertexes = std::vector<Point>();
	this->lines = std::vector<Line>();
	this->shape = new sf::ConvexShape(vertCount);
	this->location = loc;
	shape->setFillColor(color);
	shape->setPosition(loc.getX(),loc.getY());
}

Polygon::Polygon(std::vector<Point> vert, int count, sf::Color color, Vector2 loc)
{
	this->color = color;
	this->vertexCount = count;
	this->shape = new sf::ConvexShape(count);
	this->vertexes = vert;
	this->lines = std::vector<Line>();
	shape->setFillColor(color);
	for(int i =0; i< vert.size();i++)
	{
		shape->setPoint(i, sf::Vector2f(vert[i].x,vert[i].y));
	}
	this->location = loc;
	shape->setPosition(loc.getX(),loc.getY());
	
}

Polygon::Polygon(int *vert, int count, sf::Color, Vector2 loc)
{
	this->color = color;
	this->vertexCount = count;
	this->shape = new sf::ConvexShape(count);
	this->vertexes = std::vector<Point>();
	this->lines = std::vector<Line>();
	int j =0;
	for(int i =0; i< count +1;i++)
	{
		vertexes.push_back(Point(vert[j],vert[j+1]));
		j++;
	}
	shape->setFillColor(color);
	for(int i =0; i< vertexes.size();i++)
	{
		shape->setPoint(i, sf::Vector2f(vertexes[i].x,vertexes[i].y));
	}
	this->location = loc;
	shape->setPosition(loc.getX(),loc.getY());
}


Polygon::~Polygon(void)
{
	if(shape)
		delete shape;
}

void Polygon::draw(sf::RenderWindow *window)
{
	window->draw(*shape);
}


Vector2 Polygon::getCenter()
{
	return Vector2(shape->getOrigin().x,shape->getOrigin().y);
}

Vector2 Polygon::getLocation()
{
	return Vector2(shape->getPosition().x,shape->getPosition().y);
}

void Polygon::setLocation(int x, int y)
{
	location = Vector2(x,y);
}

void Polygon::setLocation(Vector2 loc)
{
	location = loc;
}

void Polygon::setLocation(sf::Vector2f loc)
{
	location = Vector2(loc.x,loc.y);
}

sf::FloatRect Polygon::getLocalBounds()
{
	return shape->getLocalBounds();
}

void Polygon::addPoint(int index, Point point)
{
	vertexes.push_back(point);
	shape->setPoint(index, sf::Vector2f(point.x,point.y));

}

void Polygon::buildLines()
{
	for(int i =0; i< vertexes.size() - 1;i++)
	{
		Point p1 = Point(location.getX() + vertexes[i].x,location.getY() + vertexes[i].y);
		Point p2 = Point(location.getX() + vertexes[i+1].x,location.getY() + vertexes[i+1].y);
		lines.push_back(Line(p1,p2));
	}
	Point p1 = Point(location.getX() + vertexes[vertexes.size() - 1].x,location.getY() + vertexes[vertexes.size() - 1].y);
	Point p2 = Point(location.getX() + vertexes[0].x,location.getY() + vertexes[0].y);
	lines.push_back(Line(p1,p2));
}

float Polygon::getMinDist(Vector2 loc, int& index, Vector2 &out)
{
	float min = -1.f;
	int ind = -1;
	Vector2 tmp = Vector2::zero();
	float counter = 0;
	for(int i = 0; i < lines.size(); i++)
	{
		if(lines[i].normalOnLine(loc))
		{
			float d = lines[i].lenNormal(loc, tmp);
			if(counter == 0)
			{
				min = d;
				ind = i;
				counter++;
			}
			if( d <= min)
			{
				min = d;
				ind = i;
				out = tmp;
			}
		}
	}
	if(ind == -1)
	{
		for(int i =0; i< vertexCount;i++)
		{
			Vector2 point = Vector2(vertexes[i].x,vertexes[i].y);
			float d = Vector2::distanceBetween(loc, point);
			if(i == 0)
			{
				min = d;
				ind = i;
			}
			if( d < min)
			{
				min = d;
				ind = i;
				out = point;
			}
		}
	}
	index = ind;
	return min;
}
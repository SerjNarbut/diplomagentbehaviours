#include "UnitGroup.hpp"

void UnitGroup::setGoalForGroup(float x, float y)
{
	rows = (int) ceilf(count / rank);
	int unit =0;
	for(int i =0; i< rows && unit < count; i++)
	{
		for(int j =0; j < rank && unit < count;j++)
		{
			units[unit++]->setGoal(x + (33*i),(y + (33*j)));
		}
	}
}

void UnitGroup::input(sf::Event &event)
{
	if(event.type == sf::Event::MouseButtonPressed)
	{
		if(event.mouseButton.button == sf::Mouse::Right)
		{
			float x = event.mouseButton.x + rand() % 4 -2;
			float y = event.mouseButton.y + rand() % 4 -2;
			setGoalForGroup(x,y);
		}
	}
}
#include <SFML\Graphics.hpp>
#include <ctime>
#include "UnitGroup.hpp"
#include "Polygon.hpp"

#define WIDTH 1300
#define HEIGHT 720
#define BLOCK_SIZE 16



int main()
{
	srand(time(0));
	sf::ContextSettings settings;
	settings.antialiasingLevel = 8;
	sf::RenderWindow window(sf::VideoMode(WIDTH,HEIGHT,32), "Diplom, nya",sf::Style::Close, settings);
	window.setVerticalSyncEnabled(true);
	window.setFramerateLimit(60);



	std::vector<Polygon*> polyVec = std::vector<Polygon*>();
	
	UnitGroup *uGroup = new UnitGroup();

	Polygon *poly1 = new Polygon(4,sf::Color(155,91,200), Vector2(300,300));
	poly1->addPoint(0,Point(0,0));
	poly1->addPoint(1,Point(64,0));
	poly1->addPoint(2,Point(64,64));
	poly1->addPoint(3,Point(0,64));

	Polygon *poly2 = new  Polygon(4,sf::Color(155,91,200), Vector2(350,350));
	poly2->addPoint(0,Point(0,0));
	poly2->addPoint(1,Point(32,0));
	poly2->addPoint(2,Point(32,32));
	poly2->addPoint(3,Point(0,32));

	Polygon *poly3 = new  Polygon(4,sf::Color(155,91,200), Vector2(400,200));
	poly3->addPoint(0,Point(0,0));
	poly3->addPoint(1,Point(32,0));
	poly3->addPoint(2,Point(47,39));
	poly3->addPoint(3,Point(0,45));

	Polygon *poly4 = new  Polygon(4,sf::Color(155,91,200), Vector2(500,500));
	poly4->addPoint(0,Point(0,0));
	poly4->addPoint(1,Point(64,0));
	poly4->addPoint(2,Point(120,70));
	poly4->addPoint(3,Point(90,120));

	Polygon *poly5= new  Polygon(4,sf::Color(155,91,200), Vector2(700,200));
	poly5->addPoint(0,Point(0,0));
	poly5->addPoint(1,Point(32,0));
	poly5->addPoint(2,Point(90,300));
	poly5->addPoint(3,Point(0,300));
	
	polyVec.push_back(poly1);
	polyVec.push_back(poly2);
	polyVec.push_back(poly3);
	polyVec.push_back(poly4);
	polyVec.push_back(poly5);
	for(int i = 0; i<polyVec.size();i++)
	{
		polyVec[i]->buildLines();
	}


	bool isRunning = true;
	bool prepareClick = false;

	while(isRunning)
	{
		sf::Event event;
		while(window.pollEvent(event))
		{
			if(event.type == sf::Event::Closed || (event.type == sf::Event::KeyPressed && event.key.code == sf::Keyboard::Escape))
			{
				isRunning = false;
			}
			if(event.type == sf::Event::MouseButtonPressed && (event.mouseButton.button == sf::Mouse::Button::Left))
			{
				prepareClick = true;
			}
			if(event.type == sf::Event::MouseButtonReleased && (event.mouseButton.button == sf::Mouse::Button::Left) && prepareClick)
			{
				prepareClick = false;
				uGroup->add(new Unit(event.mouseButton.x, event.mouseButton.y,16));
			}
			uGroup->input(event);
		}
		uGroup->update(polyVec);
		uGroup->draw(&window);
		for(int i =0; i< polyVec.size();i++)
		{
			polyVec[i]->draw(&window);
		}
		window.display();
		window.clear();
	}
	delete uGroup;
	for(int i =0; i < polyVec.size();i++)
	{
		delete polyVec[i];
	}
	window.close();

	return 0;
}
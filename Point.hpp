#pragma once
#include <cstring>
struct Point
{
	int x;
	int y;

	Point()
	{
		x = 0;
		y = 0;
	}

	Point(int x, int y)
	{
		this->x = x;
		this->y = y;
	}

	Point(const Point &other)
	{
		this->x = other.x;
		this->y = other.y;
	}

	Point& operator=(const Point &other)
	{
		this->x = other.x;
		this->y = other.y;
		return *this;
	}

	bool equals(const Point &other) const
	{
		return other.x == this->x && other.y == this->y;
	}

	std::string to_str()
	{
		return "X=" + std::to_string(x)+ " Y=" +  std::to_string(y);
	}
};
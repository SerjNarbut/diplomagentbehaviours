#pragma once
#include <vector>
#include "Unit.hpp"
#include <cstring>
#include <cmath>
#include "Polygon.hpp"


class UnitGroup
{
private:
	std::vector<Unit*> units;
	int count;
	float rank;
	int rows;

public:
	UnitGroup()
	{
		this->units = std::vector<Unit*>();
		this->count = 0;
		this->rank = 10.f;
	}

	void add(Unit *unit)
	{
		this->units.push_back(unit);
		this->count = units.size();
	}

	void input(sf::Event &event);

	void update(std::vector<Polygon*> &poly)
	{
		for(int i =0; i< units.size();i++)
		{
			units[i]->update(units, poly);
		}
	}

	void draw(sf::RenderWindow *window)
	{
		for(int i =0; i< units.size();i++)
		{
			units[i]->draw(window);
		}
	}

	void setGoalForGroup(float x, float y);

	std::string getCount()
	{
		return std::to_string(this->count);
	}

	~UnitGroup()
	{
		for(int i =0; i< units.size();i++)
		{
			delete units[i];
		}
	}
};